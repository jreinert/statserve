# Statserve

A _very_ simple web server for static file sharing

## Building

### Baked files

If you want to build a binary which already embeds the static files you want
to serve, run the following command:

```
BAKE_DIR=/path/to/public shards build --production --release --no-debug
```

`BAKE_DIR` can either be an absolute path or a path relative to the root of this
project.

### Dynamic

If you don't want to build the server on each file change or you want to serve
large files build without the `BAKE_DIR` environment variable by just running:

```
shards build --production --release --no-debug
```

## Usage

Start the server by executing the binary stored in `bin/` in the build step.

## Configuration

If you built the dynamic version you need to supply the public path to serve by
passing the `STATSERVE_PUBLIC_PATH` environment variable.

To override the default host `localhost` and port `3000` supply the environment
variables `STATSERVE_HOST` and `STATSERVE_PORT` respectively.

To enable Cache-Control headers set the `STATSERVE_CACHE_AGE` variable. The
syntax is `<value>.<unit>` e.g. `6.months` or `1.hour`.

For TLS you need to supply the environment variables `STATSERVE_SSL_CERT` and
`STATSERVE_SSL_KEY`.

To enable fallback to `index.html` on `404` just the environment variable
`STATSERVE_INDEX_FALLBACK=1` when running the binary.

## Docker

Statserve is especially useful for building self-contained docker images for
static websites or single page applications. To build such an image, write a
`Dockerfile` like the following:

``` Dockerfile
FROM node AS build
WORKDIR /opt/frontend
ADD package.json yarn.lock /opt/frontend/
RUN yarn install
ADD /src/ /opt/frontend/src/
yarn build

FROM jreinert/statserve AS embed
COPY --from=build /opt/frontend/dist/ ./public/
RUN make

FROM scratch AS final-image
COPY --from=embed /bin/statserve /bin/statserve
EXPOSE 3000
ENV STATSERVE_HOST=0.0.0.0 STATSERVE_CACHE_AGE=1.year STATSERVE_INDEX_FALLBACK=1
CMD ["/bin/statserve"]
```

You will end up with a tiny image containing the statically linked server
embedding your static website.

## Contributing

1. Fork it (<https://gitlab.com/jreinert/statserve/forks/new>)
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new Pull Request

## Contributors

- [Joakim Reinert](https://gitlab.com/jreinert) - creator and maintainer
