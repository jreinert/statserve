require "http/server"
require "logger"

{% if env("BAKE_DIR") %}
  require "./statserve/baked"
{% else %}
  require "./statserve/dynamic"
{% end %}

module Statserve
  VERSION = "0.1.6"
  {% if env("BAKE_DIR") %}
    extend Baked
  {% else %}
    extend Dynamic
  {% end %}

  class CacheHandler
    include HTTP::Handler

    @age : Time::Span

    def initialize(@age : Time::Span)
    end

    def initialize(age : Time::MonthSpan)
      @age = (age.value * 30).days
    end

    def call(context)
      context.response.headers["Cache-Control"] = "public, max-age=#{@age.total_seconds.to_u64}"
      call_next(context)
    end
  end

  class IndexFallback
    include HTTP::Handler

    def initialize(@file_handler : HTTP::Handler)
    end

    def call(context)
      return call_next(context) if context.request.path == "/index.html"
      context.request.path = "/index.html"
      @file_handler.call(context)
    end
  end

  def self.cache_handler(cache_age)
    value, unit = cache_age.split('.')
    value = value.to_i

    {% begin %}
    case unit
      {% for unit in %w[year month week day hour minute second] %}
      when {{unit}}, {{unit + "s"}}
        CacheHandler.new(value.{{unit.id}})
      {% end %}
    else
      raise "unsupported time span unit '#{unit}'"
    end
    {% end %}
  end

  def self.server
    @@server ||= begin
      handlers = [HTTP::LogHandler.new(STDOUT)] of HTTP::Handler
      cache_age = ENV["STATSERVE_CACHE_AGE"]?
      handlers << cache_handler(cache_age) unless cache_age.nil?

      {% unless env("BAKE_DIR") %}
        handlers << HTTP::CompressHandler.new
      {% end %}

      file_handler = self.file_handler
      handlers << file_handler

      case ENV["STATSERVE_INDEX_FALLBACK"]?.try(&.downcase)
      when nil, "false", "0", "no"
      else handlers << IndexFallback.new(file_handler)
      end

      HTTP::Server.new(handlers)
    end
  end

  def self.logger
    Logger.new(STDOUT)
  end

  def self.bind(host, port)
    ssl_cert = ENV["STATSERVE_SSL_CERT"]?
    ssl_key = ENV["STATSERVE_SSL_KEY"]?

    if ssl_cert && ssl_key
      ssl_context = OpenSSL::SSL::Context::Server.new.tap do |context|
        context.certificate_chain = ssl_cert
        context.private_key = ssl_key
      end

      server.bind_tls(host, port, ssl_context)
    else
      server.bind_tcp(host, port)
    end
  end

  def self.run
    host = ENV.fetch("STATSERVE_HOST", "localhost")
    port = ENV.fetch("STATSERVE_PORT", "3000").to_i

    logger.info("Starting server @ http://#{host}:#{port}")

    bind(host, port)
    server.listen
  end
end

Signal::INT.trap do
  Statserve.server.close
  exit
end

Statserve.run
